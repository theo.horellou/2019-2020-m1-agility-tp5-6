describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scenario 1', () => {
    it('charge la page de configuration', () => {
        cy.get('a[href="#configuration"]').click()
        cy.url().should('include', '/#configuration')
    })
})

describe('Scenario 2', () => {
    it('doit avoir 28 segments', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('.col-md-4 tbody').find('tr').should('have.length', 28)
    })
})

describe('Scenario 3', () => {
    it('doit avoir 0 vehicule', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('.col-md-8 tbody').find('tr').should('have.length', 1)
    })
})

describe('Scenario 4', () => {
    it('maj de la vitesse du segment', () => {
        cy.get('a[href="#configuration"]').click();
        cy.get('input[form="segment-5"][type="number"]').clear()
        cy.get('input[form="segment-5"][type="number"]').type('30');
        cy.get('form[id="segment-5"]>button').click();
        cy.request('GET', 'http://localhost:4567/elements').should((response) => {
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })
        cy.get('div[class="modal-dialog"]>div>div[class="modal-footer"]>button').click();
    })
})

describe('Scenario 5', () => {
    it('update rond point', () => {
        cy.get('a[href="#configuration"]').click();
        cy.get('.card-deck').find('.card').eq(2).find('input[name="capacity"]').clear().type('4')
        cy.get('.card-deck').find('.card').eq(2).find('input[name="duration"]').clear().type('15')
        cy.get('.card-deck').find('.card').eq(2).find("button").click()
        cy.reload()
        cy.get('a[href="#configuration"]').click()
        cy.get('.card-deck').find('.card').eq(2).find('input[name="capacity"]').should('have.value', 4)
        cy.get('.card-deck').find('.card').eq(2).find('input[name="duration"]').should('have.value', 15)
        cy.request('GET', 'http://localhost:4567/elements').should((response) => {
            expect(response.body['crossroads'][2]).to.have.property('duration', 15)
            expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
        })
    })
})

describe('Scenario 6', () => {

    it('update valeur feu', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('.card-deck').find('.card').eq(0).find('input[name="orangeDuration"]').clear().type('4')
        cy.get('.card-deck').find('.card').eq(0).find('input[name="greenDuration"]').clear().type('40')
        cy.get('.card-deck').find('.card').eq(0).find('input[name="nextPassageDuration"]').clear().type('8')
        cy.get('.card-deck').find('.card').eq(0).find("button").click()
        cy.reload()
        cy.get('a[href="#configuration"]').click()
        cy.get('.card-deck').find('.card').eq(0).find('input[name="orangeDuration"]').should('have.value', 4)
        cy.get('.card-deck').find('.card').eq(0).find('input[name="greenDuration"]').should('have.value', 40)
        cy.get('.card-deck').find('.card').eq(0).find('input[name="nextPassageDuration"]').should('have.value', 8)
        cy.request('GET', 'http://localhost:4567/elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('id', 29)
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('Scenario 7', () => {

    it('ajout 3 vehicles', () => {
        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(5);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(50);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(26);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(19);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(200);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(8);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(27);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(150);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(2);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('table:not(.table-sm) tbody tr').should('have.length', 3);
        cy.get('table:not(.table-sm) tbody tr:nth-child(1) th:nth-child(2)').contains('200.0');
        cy.get('table:not(.table-sm) tbody tr:nth-child(2) th:nth-child(2)').contains('150.0');
        cy.get('table:not(.table-sm) tbody tr:nth-child(3) th:nth-child(2)').contains('50.0');

        cy.request('GET', 'http://localhost:4567/elements').should((response) => {
            expect(response.body['50.0']).to.not.be.null;
            expect(response.body['200.0']).to.not.be.null;
            expect(response.body['150.0']).to.not.be.null;
        })
    })
})

describe('Scenario 8', () => {
    it('simu 120sec', () => {
        cy.get('a[href="#simulation"]').click();

        cy.get('table:not(.table-sm) tbody tr:nth-child(1) th:nth-child(6)').contains('block');
        cy.get('table:not(.table-sm) tbody tr:nth-child(2) th:nth-child(6)').contains('block');
        cy.get('table:not(.table-sm) tbody tr:nth-child(3) th:nth-child(6)').contains('block');

        //lancement de la simu
        cy.get('a[href="#simulation"]').click();
        cy.get('.form-control').clear();
        cy.get('.form-control').type(120);
        cy.get('.btn').click();


         //on check la barre de chargement
         cy.get('.progress-bar', {
            timeout: 9999999
        }).should('have.attr', 'aria-valuenow', '100');

        cy.get('table:not(.table-sm) tbody tr:nth-child(3) th:nth-child(6)').contains('play_circle_filled');
    })
})

describe('Scenario 9', () => {
    it('Raffraichissement de la page et simulation 500s', () => {
        cy.reload();
        cy.get('a[href="#simulation"]').click();
        cy.request('POST', 'http://127.0.0.1:4567/init').as('init');
        //on attend un chargement de la page (code 200)
        cy.get('@init').should((response) => {
            expect(response.status).to.eq(200)
        });

        //ajout des 3 vehicules
        cy.get('a[href="#configuration"]').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(5);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(50);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(26);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(19);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(200);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(8);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(27);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(150);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(2);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        //lancement de la simulation
        cy.get('a[href="#simulation"]').click();
        //actualisaiton de la simulation
        cy.get('.form-control').clear();
        cy.get('.form-control').type(500);
        cy.get('.btn').click();
        //on check la barre de chargement
        cy.get('.progress-bar', {
            timeout: 9999999
        }).should('have.attr', 'aria-valuenow', '100');
        //on verifie si tout les vehicules ne sont plus en mouvement
        cy.get('.col-md-7 > .table > tbody > :nth-child(1) > :nth-child(3)').contains('th', '0');
        cy.get('.col-md-7 > .table > tbody > :nth-child(2) > :nth-child(3)').contains('th', '0');
        cy.get('.col-md-7 > .table > tbody > :nth-child(3) > :nth-child(3)').contains('th', '0');
    });
});

describe('Scenario 10', () => {
    it('Ajout de trois vehicules + check de leur position après 200sec ', () => {
        cy.reload();
        cy.get('a[href="#simulation"]').click();
        cy.request('POST', 'http://127.0.0.1:4567/init').as('init');
        //on attend un chargement de la page (code 200)
        cy.get('@init').should((response) => {
            expect(response.status).to.eq(200)
        });
        //ajout des 3 vehicules
        cy.get('a[href="#configuration"]').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(5);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(50);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(26);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(5);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(80);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(26);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        cy.get('form[name="addVehicle"] input[type="number"][name="origin"]').clear().type(5);
        cy.get('form[name="addVehicle"] input[type="number"][name="time"]').clear().type(80);
        cy.get('form[name="addVehicle"] input[type="number"][name="destination"]').clear().type(26);
        cy.get('form[name="addVehicle"] button[type="button"]').click();
        cy.get('div.modal.show button.btn').click();

        //lancement de la simu
        cy.get('a[href="#simulation"]').click();
        cy.get('.form-control').clear();
        cy.get('.form-control').type(200);
        cy.get('.btn').click();

        //on check la barre de chargement
        cy.get('.progress-bar', {
            timeout: 9999999
        }).should('have.attr', 'aria-valuenow', '100');

        //on check la position des vehicles
        cy.get('tbody > :nth-child(1) > :nth-child(5)').contains('th', '29');
        cy.get('tbody > :nth-child(2) > :nth-child(5)').contains('th', '29');
        cy.get('tbody > :nth-child(3) > :nth-child(5)').contains('th', '17');
    });
});

